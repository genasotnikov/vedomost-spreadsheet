import { SpreadsheetVedomost, IFullLessonInfo, IFullDayInfoOfVedomost, ISubgroupLessonOfVedomost } from "./src";

export {
    SpreadsheetVedomost,
    ISubgroupLessonOfVedomost,
    IFullDayInfoOfVedomost,
    IFullLessonInfo
};