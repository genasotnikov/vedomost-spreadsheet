import { google, sheets_v4 } from "googleapis";
import request from "request";
import * as fs from "fs";
import Sheets = sheets_v4.Sheets;
import GoogleApi from "./GoogleApi";
import { IFullDayInfoOfVedomost } from "./types";

interface IVepomostGoogleSheets {
    getFileBuffer: () => Promise<string>;
}
class SpreadsheetVedomost implements IVepomostGoogleSheets {
    private init = async () => {
        await this.googleApi.auth();
        this.sheets = google.sheets({ version: "v4" });
    };

    private createDocument = async () => {
        try {
            const newDoc = await this.sheets.spreadsheets.create({
                access_token: this.googleApi.access_token,
                quotaUser: "genasotnikov@gmail.com",
            });
            this.id = newDoc.data.spreadsheetId;
        } catch (e) {
            throw new Error("Creating document failed");
        }
    };

    writeDataToDocument = async (vedomostDays: IFullDayInfoOfVedomost[]) => {
        try {
            await this.sheets.spreadsheets.batchUpdate({
                access_token: this.googleApi.access_token,
                spreadsheetId: this.id,
                requestBody: {
                    includeSpreadsheetInResponse: true,
                    requests: [
                        {
                            updateCells: {
                                start: { columnIndex: 0, rowIndex: 0 },
                                fields: "*",
                                rows: [{ values: [{ formattedValue: "123" }] }],
                            },
                        },
                    ],
                },
            });
        } catch (e) {
            throw new Error("Updating document failed. Details: " + e);
        }
    };

    downloadDocument = (savePass: string) => {
        return new Promise((resolve, reject) => {
            try {
                const vedomostFileStream = fs.createWriteStream(savePass);
                request(
                    `https://docs.google.com/spreadsheets/d/${this.id}/export?format=xlsx&id=${this.id}`,
                    {
                        headers: { Authorization: "Bearer " + this.googleApi.access_token },
                    }
                )
                    .pipe(vedomostFileStream)
                    .on("error", (e) => {
                        reject(e);
                    })
                    .on("finish", () => {
                        vedomostFileStream.close();
                        resolve("finished");
                    });
            } catch (e) {
                reject(e);
            }
        });
    };

    getFileBuffer = async (): Promise<string> => {
        await this.init();
        await this.createDocument();
        await this.writeDataToDocument([]);
        try {
            const fileName = Math.ceil(Math.random() * 100000).toString();
            const path = "tempFiles/" + fileName + ".xlsx";
            await this.downloadDocument(path);
            return String(fileName  + ".xlsx"); // await this.readDocument(path);
        } catch (e) {
            throw e;
        }
    };

    private id: string;
    private sheets: Sheets;
    private googleApi = new GoogleApi();
}

export default SpreadsheetVedomost;
