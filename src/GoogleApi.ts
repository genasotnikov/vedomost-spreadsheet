import {google} from "googleapis";
import credentials from "./googleAuth.json";

class GoogleApi {
    auth = async () => {
        const {client_email, private_key } = credentials;
        const scope = "https://www.googleapis.com/auth/spreadsheets";
        const authClient = new google.auth.JWT(client_email, null, private_key, scope); // (client_id, client_secret);
        try {
            this.access_token = (await authClient.authorize()).access_token;
        } catch (e) {
            throw new Error("123 "+ e);
        }
    };

    access_token: string;
}

export default GoogleApi;
