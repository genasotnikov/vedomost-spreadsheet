export interface ISubgroupLessonOfVedomost {
    dayIndex: number;
    yearProfile: string;
    groupSubgroup: string;
    faculty: string;
    content: string;
    lessonType: string;
}

export interface IFullLessonInfo {
    getDayIndex: number;
    addSubgroup: (data: string) => void;
    setValue: (data: ISubgroupLessonOfVedomost) => void;
}

export interface IFullDayInfoOfVedomost {
    date: Date;
    lessonsData: IFullLessonInfo[];
}