"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var googleapis_1 = require("googleapis");
var request_1 = __importDefault(require("request"));
var fs = __importStar(require("fs"));
var GoogleApi_1 = __importDefault(require("./GoogleApi"));
var SpreadsheetVedomost = /** @class */ (function () {
    function SpreadsheetVedomost() {
        var _this = this;
        this.init = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.googleApi.auth()];
                    case 1:
                        _a.sent();
                        this.sheets = googleapis_1.google.sheets({ version: "v4" });
                        return [2 /*return*/];
                }
            });
        }); };
        this.createDocument = function () { return __awaiter(_this, void 0, void 0, function () {
            var newDoc, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.sheets.spreadsheets.create({
                                access_token: this.googleApi.access_token,
                                quotaUser: "genasotnikov@gmail.com",
                            })];
                    case 1:
                        newDoc = _a.sent();
                        this.id = newDoc.data.spreadsheetId;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        throw new Error("Creating document failed");
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        this.writeDataToDocument = function () { return __awaiter(_this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.sheets.spreadsheets.batchUpdate({
                                access_token: this.googleApi.access_token,
                                spreadsheetId: this.id,
                                requestBody: {
                                    includeSpreadsheetInResponse: true,
                                    requests: [
                                        {
                                            updateCells: {
                                                start: { columnIndex: 0, rowIndex: 0 },
                                                fields: "*",
                                                rows: [{ values: [{ note: "123" }] }],
                                            },
                                        },
                                    ],
                                },
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        throw new Error("Updating document failed. Details: " + e_2);
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        this.downloadDocument = function (savePass) {
            return new Promise(function (resolve, reject) {
                try {
                    var vedomostFileStream_1 = fs.createWriteStream(savePass);
                    request_1.default("https://docs.google.com/spreadsheets/d/" + _this.id + "/export?format=xlsx&id=" + _this.id, {
                        headers: { Authorization: "Bearer " + _this.googleApi.access_token },
                    })
                        .pipe(vedomostFileStream_1)
                        .on("error", function (e) {
                        reject(e);
                    })
                        .on("finish", function () {
                        vedomostFileStream_1.close();
                        resolve("finished");
                    });
                }
                catch (e) {
                    reject(e);
                }
            });
        };
        /*
            readDocument: (path: string) => Promise<string> = (path: string) => {
                return new Promise((resolve, reject) => {
                    const chunks: Uint8Array[] = [];
                    fs.createReadStream(path)
                        .on("data", (chunk) => chunks.push(chunk))
                        .on("end", () => {
                            // fs.unlinkSync(path);
                            resolve();
                            //   resolve(Buffer.concat(chunks).toString("base64"));
                        })
                        .on("error", (e) => reject(e));
                });
            };
        */
        this.getFileBuffer = function () { return __awaiter(_this, void 0, void 0, function () {
            var fileName, path, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.init()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.createDocument()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.writeDataToDocument()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        _a.trys.push([4, 6, , 7]);
                        fileName = Math.ceil(Math.random() * 100000).toString();
                        path = "tempFiles/" + fileName + ".xlsx";
                        return [4 /*yield*/, this.downloadDocument(path)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/, String(fileName + ".xlsx")]; // await this.readDocument(path);
                    case 6:
                        e_3 = _a.sent();
                        throw e_3;
                    case 7: return [2 /*return*/];
                }
            });
        }); };
        this.googleApi = new GoogleApi_1.default();
    }
    return SpreadsheetVedomost;
}());
exports.default = SpreadsheetVedomost;
//# sourceMappingURL=SpreadsheetVedomost.js.map