declare interface SubgroupLessonOfVedomost {
    dayIndex: number;
    yearProfile: string;
    groupSubgroup: string;
    faculty: string;
    content: string;
    lessonType: string;
}

declare interface IFullLessonInfo {
    getDayIndex: number;
    addSubgroup: (data: string) => void;
    setValue: (data: SubgroupLessonOfVedomost) => void;
}

declare interface IFullDayInfoOfVedomost {
    date: Date;
    lessonsData: IFullLessonInfo[];
}

declare class SpreadsheetVedomost {
    getFileBuffer: () => Promise<string>;
    downloadDocument: (savePass: string) => Promise<void>;
    writeDataToDocument: (vedomostData: IFullDayInfoOfVedomost[]) => string;
}